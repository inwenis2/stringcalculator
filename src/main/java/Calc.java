import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.List;

public class Calc {
    public static int Sum(String input) throws InvalidParameterException {

        int sum = 0;

        if (input.equals("")) {
            return 0;
//  //[aaa]\n1aaa2aaa3 || //:\n1:2
        } else if (input.startsWith("//[")) {
            int indexOfCustomSeparatorEnd = input.lastIndexOf("]\n");
            String longCustomSeparator = input.substring(3, indexOfCustomSeparatorEnd);
            String cutInputLong = input.substring(indexOfCustomSeparatorEnd + 2);
            String[] numbersAfterLongCustomSeparator = cutInputLong.split(",|\n|" + longCustomSeparator);

            for (int i = 0; i < numbersAfterLongCustomSeparator.length; i++) {
                sum += Integer.parseInt(numbersAfterLongCustomSeparator[i]);
            }
        }


            else if (input.startsWith("//")) {
            char customSeparator = input.charAt(2);
            String cutInput = input.substring(4);
            String[] numbersAfterCustomSeparator = cutInput.split(",|\n|" + customSeparator);

            for (int i = 0; i < numbersAfterCustomSeparator.length; i++) {
                sum += Integer.parseInt(numbersAfterCustomSeparator[i]);
            }

        } else if (input.contains("-")) {

            String[] inputAfterSplit = input.split(",|\n");
            int[] numbersAsInts = new int[inputAfterSplit.length];
            String negatives = "";

            for (int i = 0; i < inputAfterSplit.length; i++) {
                numbersAsInts[i] = Integer.parseInt(inputAfterSplit[i]);
                if (numbersAsInts[i] < 0) {
                    negatives += " " + Integer.toString(numbersAsInts[i]);
                }
            }
            throw new InvalidParameterException("Negatives not allowed. Negative numbers:" + negatives);
        } else {
            String[] inputAfterSplit = input.split(",|\n");

            for (int i = 0; i < inputAfterSplit.length; i++) {
                int smallerThanThousand = Integer.parseInt(inputAfterSplit[i]);
                if (smallerThanThousand < 1000)
                    sum += smallerThanThousand;
            }
        }
        return sum;
    }
}
